package example;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import java.io.InputStream;

public class InputStreamCustomSerializer extends StdSerializer<InputStream> {
    protected InputStreamCustomSerializer(Class<InputStream> t) {
        super(t);
    }

    public InputStreamCustomSerializer() {
        this(null);
    }

    @Override
    public void serialize(InputStream inputStream, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("inputStream", inputStream.toString());
        jsonGenerator.writeEndObject();
    }
}
