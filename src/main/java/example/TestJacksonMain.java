package example;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.nio.channels.Channels;
import java.nio.file.Files;
import java.nio.file.Path;
import org.springframework.core.io.FileSystemResource;

public class TestJacksonMain {
    public static void main(String[] args) throws IOException, URISyntaxException {
        ObjectMapper objectMapper = new ObjectMapper();
        //objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        SimpleModule module = new SimpleModule();
        module.addSerializer(InputStream.class, new InputStreamCustomSerializer());
        module.addSerializer(Channels.class, new ChannelsCustomSerializer());
        module.addSerializer(OutputStream.class, new OutputStreamCustomSerializer());
        objectMapper.registerModule(module);

        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        File tempFile = File.createTempFile("tempTest", ".txt");
        FileOutputStream tempFileOutputStream = new FileOutputStream(tempFile);
        Files.copy(Path.of(classLoader.getResource("test.txt").toURI()), tempFileOutputStream);

        FileSystemResource fileSystemResource = new FileSystemResource(tempFile);
        System.out.println("Before: " + fileSystemResource.getInputStream().available());
        objectMapper.writeValueAsString(fileSystemResource);
        System.out.println("After: " + fileSystemResource.getInputStream().available());
    }
}
