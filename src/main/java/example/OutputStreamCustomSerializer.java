package example;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import java.io.OutputStream;

public class OutputStreamCustomSerializer extends StdSerializer<OutputStream> {
    protected OutputStreamCustomSerializer(Class<OutputStream> t) {
        super(t);
    }

    public OutputStreamCustomSerializer() {
        this(null);
    }

    @Override
    public void serialize(OutputStream outputStream, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("outputStream", outputStream.toString());
        jsonGenerator.writeEndObject();
    }
}
