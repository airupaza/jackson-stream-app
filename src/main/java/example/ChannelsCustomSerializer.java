package example;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import java.nio.channels.Channels;

public class ChannelsCustomSerializer extends StdSerializer<Channels> {
    protected ChannelsCustomSerializer(Class<Channels> t) {
        super(t);
    }

    public ChannelsCustomSerializer() {
        this(null);
    }

    @Override
    public void serialize(Channels channels, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("channels", channels.toString());
        jsonGenerator.writeEndObject();
    }
}
